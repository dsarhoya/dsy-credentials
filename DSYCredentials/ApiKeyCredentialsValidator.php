<?php

namespace DSYCredentials;

/**
 * Description of ApiKeyCredentialsValidator
 *
 * @author matias
 */
class ApiKeyCredentialsValidator {
    
    private $key_id;
    private $key;
    private $timestamp;
    private $url_string;
    private $signature;
    private $post_parameters;
    
    private $valid = null;
    
    public function __construct($url_string, $post_parameters) {
        $this->url_string   = $url_string;
        $this->post_parameters = $post_parameters;
        $this->processUrlString();
    }
    
    public function getKeyId(){
        return $this->key_id;
    }
    
    public function getKey(){
        return $this->key;
    }
    
    private function processUrlString(){
        
        $path_array = explode("?",  $this->url_string);
        if(!isset($path_array[1])){
            $this->valid = false;
            return;
        }
        
        foreach (explode("&",  $path_array[1]) as $value) {
            $aux_value = explode("=", $value);
            if((string)$aux_value[0]==ApiKeyConstants::CREDENTIALS_KEY_SIGNATURE){
                $this->signature = (string)$aux_value[1];
                continue;
            }
            if((string)$aux_value[0]==ApiKeyConstants::CREDENTIALS_KEY_ID){
                $this->key_id = (string)$aux_value[1];
                continue;
            }
            if((string)$aux_value[0]==ApiKeyConstants::CREDENTIALS_KEY_TIMESTAMP){
                $this->timestamp = (string)$aux_value[1];
                continue;
            }
        }
        
        if(is_null($this->signature)) $this->valid = false;
        if(is_null($this->key_id)) $this->valid = false;
        if(is_null($this->timestamp)) $this->valid = false;
        
    }
    
        
    public function setKey($key){
        $this->key = $key;
        $this->setKeyIdAndSecret($key->getId(), $key->getSecret());
    }
    
    public function setKeyIdAndSecret($keyId, $secret){
        
        if($this->valid===false) return ;
        
        
        $credencials = new ApiKeyCredentialsGenerator($this->url_string, $keyId, $secret, $this->timestamp, $this->post_parameters);
        $correct_signature = $credencials->getSignature();
        
        if($correct_signature != $this->signature){
            $this->valid = false;
            return;
        }

        if(abs(time() - $this->timestamp) > ApiKeyConstants::TIME_VALIDITY_IN_SECONDS){
            $this->valid = false;
            return;
        }
        
        $this->valid = true;
        return $this->valid;
        
    }
    
    public function areValid(){
        return $this->valid;
    }
}
