<?php

namespace DSYCredentials;

/**
 * Description of ApiKeyConstants
 *
 * @author matias
 */
class ApiKeyConstants {
    
    CONST TIME_VALIDITY_IN_SECONDS = 7800;
    
    CONST CREDENTIALS_KEY_SIGNATURE     = 'signature';
    CONST CREDENTIALS_KEY_ID            = 'key_id';
    CONST CREDENTIALS_KEY_TIMESTAMP     = 'ts';
    
}
