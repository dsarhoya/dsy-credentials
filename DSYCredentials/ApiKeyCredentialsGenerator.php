<?php

namespace DSYCredentials;

/**
 * Description of ApiKeyCredentialsGenerator
 *
 * @author matias
 */
class ApiKeyCredentialsGenerator {
    
    private $key;
    private $timestamp;
    private $url_string;
    private $secret;
    private $signature;
    private $has_parameters = false;
    private $post_parameters;
    
    public function __construct($url_string, $key, $secret, $timestamp = null, $post_parameters) {
        $this->url_string   = $url_string;
        $this->key          = $key;
        $this->secret       = $secret;
        $this->post_parameters  = $post_parameters;
        $this->generateSignature($timestamp);
    }
    
    public function getSignature(){
        return $this->signature;
    }
    
    public function getTimestamp(){
        return $this->timestamp;
    }
    
    public function getKey(){
        return $this->key;
    }
    public function getSignedURL(){
        $connector = ($this->has_parameters) ? "&":"?";
    return $this->url_string.$connector."signature=".$this->getSignature()."&key_id=".$this->getKey()."&ts".$this->getTimeStamp();
    }
    
    private function generateSignature($timestamp = null){
        
        if(is_null($timestamp)) $timestamp = time();
        
        $this->timestamp = $timestamp;
        
        $signature_array = array();
        $path_array = explode("?",  urldecode($this->url_string));
        $signature_array[] = (string)$path_array[0];
        
        if(isset($path_array[1])){
        $this->has_parameters = true; 
            foreach (explode("&",  $path_array[1]) as $value) {
                $aux_value = explode("=", $value);
                if((string)$aux_value[0]==ApiKeyConstants::CREDENTIALS_KEY_SIGNATURE) continue;
                if((string)$aux_value[0]==ApiKeyConstants::CREDENTIALS_KEY_ID) continue;
                if((string)$aux_value[0]==ApiKeyConstants::CREDENTIALS_KEY_TIMESTAMP) continue;
                $signature_array[] = (string)$aux_value[0];
                $signature_array[] = (string)(isset($aux_value[1]) ? $aux_value[1] : '');
            }
        }
        
        if(is_array($this->post_parameters) && count($this->post_parameters) > 0){
            foreach ($this->post_parameters as $name => $value) {
                $signature_array[] = (string)$name;
                $signature_array[] = (string)$value;
            }
        }
        
        sort($signature_array, SORT_STRING);
        
        $signature_string = implode('', $signature_array);
        $signature_string .= $this->key;
        $signature_string .= $this->secret;
        $signature_string .= $this->timestamp;
        $this->signature = md5($signature_string);
        
        return true;
    }
}
